package com.atlassian.bamboo.plugins.cppunit;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.atlassian.bamboo.build.test.TestReportProvider;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Set;

public class CppUnitTestReportCollector implements TestReportCollector
{
    @NotNull
    @Override
    public TestCollectionResult collect(@NotNull File file) throws Exception
    {
        final CppUnitParser parser = new CppUnitParser();

        InputStream is = null;
        try
        {
            is = new FileInputStream(file);
            parser.parse(is);
        }
        finally
        {
            IOUtils.closeQuietly(is);
        }

        return new TestCollectionResultBuilder()
                .addFailedTestResults(parser.getFailedTests())
                .addSuccessfulTestResults(parser.getPassedTests()).build();
    }

    @NotNull
    @Override
    public Set<String> getSupportedFileExtensions()
    {
        return Sets.newHashSet("xml");
    }
}
