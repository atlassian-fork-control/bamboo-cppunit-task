package com.atlassian.bamboo.plugins.cppunit;

import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultError;
import com.atlassian.bamboo.resultsummary.tests.TestClass;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Lists;
import junit.framework.TestCase;

import java.io.InputStream;
import java.util.List;
import java.util.Iterator;

public class CppUnitParserTest extends TestCase
{
    public void testReportFile() throws Exception
    {
        CppUnitParser parser = new CppUnitParser();

        InputStream is = getClass().getResourceAsStream("/testreport.xml");

        parser.parse(is);

        Iterator<TestResults> iter = parser.getFailedTests().iterator();
        assertEquals(2, parser.getFailedTests().size());
        assertEquals(9, parser.getPassedTests().size());

        TestResults failingTest = iter.next();
        assertEquals("AClass", failingTest.getClassName());
        assertEquals("test2", failingTest.getActualMethodName());
        assertEquals(TestState.FAILED, failingTest.getState());
        assertEquals(1, failingTest.getErrors().size());

        TestCaseResultError error = failingTest.getErrors().get(0);

        assertEquals("uncaught exception of unknown type\n            ", error.getContent());

        failingTest = iter.next();
        assertEquals("AClass", failingTest.getClassName());
        assertEquals("test4", failingTest.getActualMethodName());
        assertEquals(TestState.FAILED, failingTest.getState());
        assertEquals(1, failingTest.getErrors().size());

        error = failingTest.getErrors().get(0);

        assertEquals("assertion failed\n                - Expression: rsc != 0\n            ", error.getContent());

        List<TestResults> successfulTests = Lists.newLinkedList(parser.getPassedTests());

        TestResults successfulTest = successfulTests.get(0);

        assertEquals("AClass", successfulTest.getClassName());
        assertEquals("test1", successfulTest.getActualMethodName());
    }
}
